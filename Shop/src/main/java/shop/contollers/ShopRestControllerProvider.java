package shop.contollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import shop.structure.Article;
import shop.structure.Customer;
import shop.structure.Order;
import java.util.ArrayList;

@EnableWebMvc
@RestController
public class ShopRestControllerProvider {

    @Autowired
    ShopRestControllerRequester shopRestControllerRequester;

    @RequestMapping()
    public String greetUser() {
        return "Welcome to Shop Service";
    }

    //ARTICLE
    @RequestMapping(method = RequestMethod.GET, value = "/listallarticles")
    public ArrayList<Article> getArticles() {
        return shopRestControllerRequester.getArticles();
    }
    @RequestMapping(method = RequestMethod.GET, value = "/searcharticle/{id}")
    public Article getArticleById(@PathVariable Long id) {
        return shopRestControllerRequester.getArticleById(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/savearticle")
    public void saveNewArticle(@RequestBody Article article) {
        shopRestControllerRequester.saveNewArticle(article);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/deletearticle/{id}")
    public void deleteArticleById(@PathVariable long id) {
        shopRestControllerRequester.deleteArticleById(id);
    }

    //CUSTOMER
    @RequestMapping(method = RequestMethod.GET, value = "/searchcustomer/{id}")
    public Customer getCustomerById(@PathVariable Long id) {
        return shopRestControllerRequester.getCustomerById(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/listallcustomers")
    public ArrayList<Customer> getCustomers() {
        return shopRestControllerRequester.getCustomers();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/deletecustomer/{id}")
    public void deleteCustomer(@PathVariable long id) {
       shopRestControllerRequester.deleteCustomerById(id);
    }


    @RequestMapping(method = RequestMethod.POST, value = "/savecustomer")
    public void saveNewCustomer(@RequestBody Customer customer) {
        shopRestControllerRequester.saveNewCustomer(customer);
    }

    //ORDER
    @RequestMapping(method = RequestMethod.GET, value = "/searchorder/{id}")
    public Order getOrderById(@PathVariable Long id) {
        return shopRestControllerRequester.getOrderById(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/listallorders")
    public ArrayList<Order> getOrders() {
        return shopRestControllerRequester.getOrders();
    }

    @RequestMapping(method = RequestMethod.POST, value = "/saveorder")
    public void saveNewOrder(@RequestBody Order order) {
        shopRestControllerRequester.saveNewOrder(order);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/deleteorder/{id}")
    public void deleteOrderById(@PathVariable long id) {
         shopRestControllerRequester.deleteOrderById(id);
    }

}
