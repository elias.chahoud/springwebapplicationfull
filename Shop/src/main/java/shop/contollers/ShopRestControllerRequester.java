package shop.contollers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.weaver.ast.Or;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import shop.structure.Article;
import shop.structure.Customer;
import shop.structure.Order;

import java.util.ArrayList;

@RestController
@RequestMapping
public class ShopRestControllerRequester {

    RestTemplate restTemplate = new RestTemplate();

    @SuppressWarnings("unchecked")
    public ArrayList<Article> getArticles() {
        return restTemplate.getForObject("http://apigateway:8090/article/listall", ArrayList.class);
    }

    //ARTICLE
    public Article getArticleById(Long id) {
        return restTemplate.getForObject("http://apigateway:8090/article/search/" + id, Article.class);
    }

    public Article saveNewArticle(Article article) {
        return restTemplate.postForObject("http://apigateway:8090/article/save/", article, Article.class);
    }

    public void deleteArticleById(Long id) {
        restTemplate.delete("http://apigateway:8090/article/delete/" + id);
    }

    //CUSTOMER
    public Customer getCustomerById(Long id) {
        return restTemplate.getForObject("http://apigateway:8090/customer/search/" + id, Customer.class);
    }

    public void deleteCustomerById(Long id) {
        restTemplate.delete("http://apigateway:8090/customer/delete/" + id);
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Customer> getCustomers() {
        return restTemplate.getForObject("http://apigateway:8090/customer/listall", ArrayList.class);
    }

    public Customer saveNewCustomer(Customer customer) {
        return restTemplate.postForObject("http://apigateway:8090/customer/save/", customer, Customer.class);
    }

    //ORDER
    public Order getOrderById(Long id) {
        return restTemplate.getForObject("http://apigateway:8090/order/search/" + id, Order.class);
    }

    @SuppressWarnings("unchecked")
    public ArrayList<Order> getOrders() {
        return restTemplate.getForObject("http://apigateway:8090/order/listall", ArrayList.class);
    }

    public void deleteOrderById(Long id) {
        restTemplate.delete("http://apigateway:8090/order/delete/" + id);
    }


    public Order saveNewOrder(Order order) {
        return restTemplate.postForObject("http://apigateway:8090/order/save/", order, Order.class);
    }
}
