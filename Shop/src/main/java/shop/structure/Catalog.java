package shop.structure;

import java.util.ArrayList;


public class Catalog {

	private Long catalogId;
	private ArrayList<Long> articles;

	public Catalog() {
		articles = new ArrayList<Long>();
	}

	public Long getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(Long catalogId) {
		this.catalogId = catalogId;
	}
	public ArrayList<Long> getArticles() {
		return articles;
	}
	public void setArticles(ArrayList<Long> articles) {
		this.articles = articles;
	}

}
