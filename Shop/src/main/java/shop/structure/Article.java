
package shop.structure;

import javax.persistence.*;

@Entity
@Table(name = "article_table")
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String manufactor;
	private String name;
	private float price;

	public Article() {
	}

	public Article(String name, String manufactor, float price) {
		this.name = name;
		this. manufactor = manufactor;
		this.price = price;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long articleId) {
		this.id = articleId;
	}

	public String getManufactor() {
		return manufactor;
	}

	public void setManufactor(String manufactor) {
		this.manufactor = manufactor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}