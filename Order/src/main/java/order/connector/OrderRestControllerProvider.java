package order.connector;

import order.behaviour.OrderService;
import order.structure.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OrderRestControllerProvider {

    @Autowired
    private OrderService orderService;

    @RequestMapping()
    public String greetUser(){
        return "Welcome to Order Service";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/listall")
    public List<Order> getAllOrders() {
        return orderService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/search/{id}")
    public Order getOrder(@PathVariable Long id) {
        return orderService.findById(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public void addOrder(@RequestBody Order order) {
        orderService.save(order);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public void deleteOrder(@PathVariable Long id) {
        orderService.deleteById(id);
    }

    // {
    //
    //
    // }

}
