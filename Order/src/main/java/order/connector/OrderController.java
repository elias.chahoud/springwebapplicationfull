package order.connector;

import order.repositories.OrderRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

//          *******************Connector == Controller*******************
@Controller
public class OrderController {


    private final OrderRepository orderRepository;

    public OrderController(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @RequestMapping(value = "/orders")
    public String getOrders(Model model) {

        model.addAttribute("orders",orderRepository.findAll());

        return "orders/list";
    }
}
