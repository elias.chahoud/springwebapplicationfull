package order.behaviour;

import order.repositories.OrderRepository;
import order.structure.Order;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BootStrapData implements CommandLineRunner {

    private final OrderRepository orderRepository;

    public BootStrapData(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public void run(String... args) throws Exception {



        Order firstOrder = new Order("ChairOrder","first Order","1,2");
        Order secondOrder = new Order("PcOrder","second Order","2,3");


        orderRepository.save(firstOrder);
        orderRepository.save(secondOrder);

        System.out.println("Started with BootStrap!");
        System.out.println("number of orders: " + orderRepository.count());


    }
}