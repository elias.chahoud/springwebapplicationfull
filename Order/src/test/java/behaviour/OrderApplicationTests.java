package behaviour;

import order.structure.Order;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootApplication
class OrderApplicationTest {

	Order order;

	@BeforeEach
	public void setup() {
		order = new Order("test", "test", "1,2");
	}

	@Test
	public void areCustomersSaved() {
		Assert.assertNotNull(order.getName());
	}
}