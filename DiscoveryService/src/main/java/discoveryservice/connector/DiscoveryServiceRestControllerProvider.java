package discoveryservice.connector;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@RestController
public class DiscoveryServiceRestControllerProvider {


    @RequestMapping
    public String greetUser(){
        return "Welcome to Discovery Service";
    }
}
