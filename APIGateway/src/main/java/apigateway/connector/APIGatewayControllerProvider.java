package apigateway.connector;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@RestController
public class APIGatewayControllerProvider {

    @RequestMapping
    public String greetUser(){
        return "Welcome to API Gateway Service";
    }


}
