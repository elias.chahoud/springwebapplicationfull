package article.behaviour;

import article.repositories.ArticleRepository;
import article.structure.Article;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
class ArticleApplicationTest {

	Article article;

	@Autowired
	private ArticleRepository articleRepository;

	@BeforeEach
	void setup(){
		article = new Article("test", "test", 9f);
	}
	@Test
	public void areArticlesFound() {
		Assert.assertNotNull(articleRepository.findAll());
	}

	@Test
	public void areArticlesFoundById() {
		Assert.assertNotNull(articleRepository.findById(1L));
	}

	@Test
	public void areArticlesSaved() {
		Assert.assertNotNull(articleRepository.save(article));
	}

}
