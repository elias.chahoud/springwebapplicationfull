package article.connector;

import article.repositories.ArticleRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

//          *******************Connector == Controller*******************
@Controller
public class ArticleController {


    private final ArticleRepository articleRepository;

    public ArticleController(ArticleRepository articleRepository) {
        this.articleRepository = articleRepository;
    }

    @RequestMapping(value = "/listarticles")
    public String getArticles(Model model) {

        model.addAttribute("articles",articleRepository.findAll());

        return "articles/list";
    }

    @RequestMapping(value ="/articles/?id")
    public String getArticles(@PathVariable("id") Long id, Model model) {

        model.addAttribute("articles",articleRepository.findById(id));

        return "articles/listid";
    }
}
