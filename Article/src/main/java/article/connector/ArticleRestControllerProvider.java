package article.connector;

import article.behaviour.ArticleService;
import article.repositories.ArticleRepository;
import article.structure.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.ArrayList;
import java.util.List;

@EnableWebMvc
@RestController
public class ArticleRestControllerProvider {

    @Autowired
    private ArticleService articleService;

    ArticleRepository articleRepository;

    @RequestMapping
    public String greetUser(){
        return "Welcome to Article Service";
    }


    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public void addArticle(@RequestBody Article article) {
         articleService.save(article);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public void addOrder(@PathVariable long id) {
        articleService.deleteById(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/listall")
    public List<Article> getAllArticles() {
        return articleService.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/search/{id}")
    public Article getOrder(@PathVariable Long id) {
        return articleService.findById(id);
    }

}
