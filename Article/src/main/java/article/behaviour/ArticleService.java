package article.behaviour;

import article.repositories.ArticleRepository;
import article.structure.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public ArrayList<Article> findAll() {
        return (ArrayList<Article>)articleRepository.findAll();
    }

    public Article findById(Long id) {
        return articleRepository.findById(id).orElse(null);
    }

    public void deleteById(Long id) {
        articleRepository.deleteById(id);
    }

    public void save(Article article) {
        articleRepository.save(article);
    }

    public ArrayList<Article> findByIds(ArrayList<Long> articles) {
        return (ArrayList<Article>)articleRepository.findAllById(articles);
    }
}
