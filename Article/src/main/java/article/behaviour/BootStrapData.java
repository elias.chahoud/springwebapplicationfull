package article.behaviour;

import article.structure.Article;
import article.structure.Order;
import article.repositories.ArticleRepository;
import article.repositories.OrderRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootStrapData implements CommandLineRunner {

    private final ArticleRepository articleRepository;
    private final OrderRepository orderRepository;

    public BootStrapData(ArticleRepository articleRepository, OrderRepository orderRepository) {
        this.articleRepository = articleRepository;
        this.orderRepository = orderRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Article firstArticle = new Article("Chair","Leuphana",9.99f);
        Article secondArticle = new Article("PC","Leuphana",567f);
        Article thirdArticle = new Article("Keyboard","Leuphana",16.49f);
        Order order = new Order("ChairOrder","first Order");

        articleRepository.save(firstArticle);
        articleRepository.save(secondArticle);
        articleRepository.save(thirdArticle);
        orderRepository.save(order);

        System.out.println("Started with BootStrap!");
        System.out.println("number of articles " + articleRepository.count());
        System.out.println("number of orders: " + orderRepository.count());
    }
}
