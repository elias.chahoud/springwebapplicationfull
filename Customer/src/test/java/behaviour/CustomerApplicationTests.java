package behaviour;

import customer.repositories.CustomerRepository;
import customer.structure.Customer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootApplication
class CustomerApplicationTest {

	Customer customer;

	@BeforeEach
	public void setup() {
		customer = new Customer("test", "test", 1L);
	}

	@Test
	public void areCustomersSaved() {
		Assert.assertNotNull(customer.getName());
	}
}