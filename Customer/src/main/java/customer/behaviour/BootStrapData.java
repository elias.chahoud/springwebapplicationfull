package customer.behaviour;


import customer.repositories.CustomerRepository;
import customer.structure.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class BootStrapData implements CommandLineRunner {


    private final CustomerRepository customerRepository;

    public BootStrapData(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        List<Long> list = new ArrayList<>();
        list.add(1L);
        list.add(2L);
        list.add(3L);

        Customer customer1 = new Customer("Elias","Lüneburg");
        Customer customer2 = new Customer("Rico","Winsen",1L);
        Customer customer3 = new Customer("Philipp","kein Ahnung",list);

        customerRepository.save(customer1);
        customerRepository.save(customer2);
        customerRepository.save(customer3);

        System.out.println("Started with BootStrap!");
        System.out.println("number of customers " + customerRepository.count());

    }

}
