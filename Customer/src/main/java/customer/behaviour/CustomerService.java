package customer.behaviour;

import customer.repositories.CustomerRepository;
import customer.structure.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAll() {
        return (List<Customer>)customerRepository.findAll();
    }

    public Customer findById(Long Id) {
        return customerRepository.findById(Id).orElse(null);
    }

    public Customer saveCustomers(Customer customer) {
        return customerRepository.save(customer);
    }

    public void deleteCustomer(Long Id) {
        customerRepository.deleteById(Id);
    }
}
