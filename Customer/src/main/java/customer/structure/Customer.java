package customer.structure;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "customer_table")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String address;


	@Column(name = "ORDERID")
	@ElementCollection
	private List<Long> orderId = new ArrayList<Long>();

    public Customer() {
    }

    public Customer(String name, String address) {
        this.name = name;
        this.address = address;
    }

	public Customer(String name, String address,Long orderId) {
		this.name = name;
		this.address = address;
		this.orderId.add(orderId);
	}

	public Customer(String name, String address,List<Long> orderId) {
		this.name = name;
		this.address = address;
		this.orderId =orderId;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}