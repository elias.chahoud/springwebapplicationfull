package customer.connector;

import customer.behaviour.CustomerService;
import customer.repositories.CustomerRepository;
import customer.structure.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;
@EnableWebMvc
@RestController
public class CustomerRestControllerProvider {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerRepository customerRepository;

    public CustomerRestControllerProvider(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping()
    public String greetUser(){
        return "Welcome to Customer Service";
    }

    @GetMapping("/listall")
    public List<Customer> listAllCustomers() {
        return (List<Customer>) customerRepository.findAll();
    }

    @GetMapping("/search/{id}")
    public Customer search(@PathVariable Long id) {
        return customerService.findById(id);
    }

    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{id}")
    public void deleteCustomer(@PathVariable long id){
        customerRepository.deleteById(id);
    }
}
