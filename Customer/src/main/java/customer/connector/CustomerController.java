package customer.connector;

import customer.repositories.CustomerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


@Controller
public class CustomerController {


    private final CustomerRepository customerRepository;

    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(value = "/listcustomers")
    public String getCustomers(Model model){

        model.addAttribute("customers",customerRepository.findAll());

        return "customers/list";
    }

}
